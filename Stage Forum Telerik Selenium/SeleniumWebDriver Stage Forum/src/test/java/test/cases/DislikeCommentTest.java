package test.cases;

import com.telerikacademy.testframework.forum.LoginPage;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class DislikeCommentTest extends BaseTestSetup {

    private WebDriverWait wait;

    public void DisikeCommentTest() {
    }

    @Test
    public void dislikeComment_onTopic_test() throws InterruptedException {
        LoginPage loginPage = new LoginPage(actions.getDriver(), "home.page");
        loginPage.loginUser();

        actions.waitForElementVisible("search.button");
        actions.waitForElementClickable("search.button");
        actions.clickElement("search.button");

        actions.waitForElementVisible("search.dropdown");
        actions.waitForElementClickable("search.dropdown");
        actions.clickElement("search.dropdown");
        actions.typeValueInField("Alpha 38 QA - We are awesome and great", "search.dropdown");
        actions.pressKey(Keys.ENTER);
        actions.waitForElementVisible("search.result");
        actions.waitForElementClickable("search.result");
        actions.clickElement("search.result");

        //page needs more time to load
        wait = new WebDriverWait(actions.getDriver(), Duration.ofSeconds(8000));
        JavascriptExecutor js = (JavascriptExecutor) actions.getDriver();
        js.executeScript("window.scrollBy(0,1500)");

        actions.waitForElementVisible("like.button");
        actions.waitForElementClickable("like.button");
        actions.clickElement("like.button");

    }
}
