package test.cases;

import com.telerikacademy.testframework.forum.LoginPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class CreateTopicTest extends BaseTestSetup {
    public CreateTopicTest() {

    }

    @Test
    public void testLogin() {
        LoginPage loginPage = new LoginPage(actions.getDriver(), "home.page");
        loginPage.loginUser();
    }

    @Test
    public void createTopic() {
        LoginPage loginPage = new LoginPage(actions.getDriver(), "home.page");
        loginPage.loginUser();

        actions.waitForElementVisible("newtopic.button");
        actions.waitForElementClickable("newtopic.button");
        actions.clickElement("newtopic.button");

        actions.waitForElementVisible("title.topic");
        actions.waitForElementClickable("title.topic");
        actions.clickElement("title.topic");
        actions.typeValueInField("I am testing title topic", "title.topic");

        actions.waitForElementVisible("desc.topic");
        actions.waitForElementClickable("desc.topic");
        actions.clickElement("desc.topic");
        actions.typeValueInField("I am testing the description", "desc.topic");

        actions.waitForElementVisible("topic.creation.button");
        actions.waitForElementClickable("topic.creation.button");
        actions.clickElement("topic.creation.button");

        //validate topic creation
        actions.getDriver().navigate().to("home.page");
        actions.waitForElementVisible("search.button");
        actions.waitForElementClickable("search.button");
        actions.clickElement("search.button");

        actions.waitForElementVisible("search.dropdown");
        actions.waitForElementClickable("search.dropdown");
        actions.clickElement("search.dropdown");
        actions.typeValueInField("I am testing title topic", "search.dropdown");
        actions.pressKey(Keys.ENTER);
        actions.waitForElementVisible("search.result");
        actions.waitForElementClickable("search.result");
        actions.clickElement("search.result");

        Assert.assertTrue("Topic is present", true);

    }
}
