package test.cases;

import com.telerikacademy.testframework.forum.LoginPage;
import org.checkerframework.checker.units.qual.K;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class LikeTopicTest extends BaseTestSetup {

    private WebDriverWait wait;

    public LikeTopicTest() {
    }

    @Test
    public void likeComment_onTopic_test() throws InterruptedException {
        LoginPage loginPage = new LoginPage(actions.getDriver(), "home.page");
        loginPage.loginUser();

        actions.waitForElementVisible("search.button");
        actions.waitForElementClickable("search.button");
        actions.clickElement("search.button");

        actions.waitForElementVisible("search.dropdown");
        actions.waitForElementClickable("search.dropdown");
        actions.clickElement("search.dropdown");
        actions.typeValueInField("Alpha 38 QA - We are awesome and great", "search.dropdown");
        actions.pressKey(Keys.ENTER);
        actions.waitForElementVisible("search.result");
        actions.waitForElementClickable("search.result");
        actions.clickElement("search.result");

        //page needs more time to load
        wait = new WebDriverWait(actions.getDriver(), Duration.ofSeconds(8000));
        JavascriptExecutor js = (JavascriptExecutor) actions.getDriver();
        js.executeScript("window.scrollBy(0,1500)");

        actions.waitForElementVisible("like.button");
        actions.waitForElementClickable("like.button");
        actions.clickElement("like.button");

        //validate comment is liked
        actions.getDriver().navigate().to("user.activity.url");
        actions.waitForElementVisible("likes.user.activity");
        actions.waitForElementClickable("likes.user.activity");
        actions.clickElement("likes.user.activity");

        Assert.assertTrue("Alpha 38 QA - We are awesome and great", true);

    }
}