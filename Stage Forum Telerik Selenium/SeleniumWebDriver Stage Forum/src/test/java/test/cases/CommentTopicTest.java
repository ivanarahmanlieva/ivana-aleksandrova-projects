package test.cases;

import com.telerikacademy.testframework.forum.LoginPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;


public class CommentTopicTest extends BaseTestSetup {
    public CommentTopicTest() {
    }

    @Test
    public void createComment_onTopic_test() {
        LoginPage loginPage = new LoginPage(actions.getDriver(), "home.page");
        loginPage.loginUser();

        actions.waitForElementVisible("search.button");
        actions.waitForElementClickable("search.button");
        actions.clickElement("search.button");

        actions.waitForElementVisible("search.dropdown");
        actions.waitForElementClickable("search.dropdown");
        actions.clickElement("search.dropdown");
        actions.typeValueInField("Alpha 38 QA - We are awesome and great", "search.dropdown");
        actions.pressKey(Keys.ENTER);
        actions.waitForElementVisible("search.result");
        actions.waitForElementClickable("search.result");
        actions.clickElement("search.result");

        actions.pressKey(Keys.PAGE_DOWN);
        actions.waitForElementPresent("reply.button");
        actions.waitForElementClickable("reply.button");
        actions.waitForElementClickable("reply.button");
        actions.clickElement("reply.button");

        actions.waitForElementPresent("comment.box");
        actions.waitForElementClickable("comment.box");
        actions.clickElement("comment.box");
        actions.typeValueInField("Hi, I'm Ivana, and I did it :smile: :partying_face: :beers: :see_no_evil: " +
                ":hear_no_evil: :speak_no_evil: :heart: ", "comment.box");

        actions.pressKey(Keys.TAB);
        actions.pressKey(Keys.ENTER);

        //validate comment creation
        actions.getDriver().navigate().to("user.activity.url");
        actions.waitForElementVisible("element.visibility.xpath");
        actions.isElementPresent("element.visibility.xpath");
        Assert.assertTrue("Comment in Alpha 38 QA - We are awesome and great is visible", true);

    }
}
