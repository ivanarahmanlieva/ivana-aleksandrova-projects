package com.telerikacademy.testframework.forum;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage {

    public LoginPage(WebDriver driver, String url) {
        super(driver, url);
    }


    public void loginUser() {
        actions.waitForElementVisible("forum.login.button");
        actions.waitForElementClickable("forum.login.button");
        actions.clickElement("forum.login.button");

        actions.waitForElementVisible("email.user.textbox");
        actions.waitForElementClickable("email.user.textbox");
        actions.clickElement("email.user.textbox");
        //enter email
        actions.typeValueInField("","email.user.textbox");

        actions.waitForElementClickable("password.user.textbox");
        actions.waitForElementClickable("password.user.textbox");
        actions.clickElement("password.user.textbox");
        //enter password
        actions.typeValueInField("","password.user.textbox");

        actions.waitForElementClickable("login.final.button");
        actions.clickElement("login.final.button");

    }
}
