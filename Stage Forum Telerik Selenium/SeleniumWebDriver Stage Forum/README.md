**Instructions**
1. Clone repository
2. Open `SeleniumWebDriver` as a IntelliJ IDEA Project
3. Build
4. Run tests in logical order

Username and password must be added manually.
Validation is done in user profile. Should be changed when testing the assertions.