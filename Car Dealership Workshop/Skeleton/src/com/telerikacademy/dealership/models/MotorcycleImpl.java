package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Motorcycle;


public class MotorcycleImpl extends VehicleBase implements Motorcycle {

    //look in DealershipFactoryImpl - use it to create proper constructor
    private static final int wheels = 2;
    private final int MIN_CATEGORY_LENGTH = 3;
    private final int MAX_CATEGORY_LENGTH = 10;
    private String category;

    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price, VehicleType.MOTORCYCLE, wheels);
        setCategory(category);
    }

    @Override
    public String getCategory() {
        return category;
    }

    private String setCategory(String category) {
        if (category == null || category.isEmpty()) {
            throw new NullPointerException("Category can not be null or empty!");
        }
        if (category.length() <= MIN_CATEGORY_LENGTH || category.length() >= MAX_CATEGORY_LENGTH) {
            throw new IllegalArgumentException("Category must be between " +
                    MIN_CATEGORY_LENGTH + " and " + MAX_CATEGORY_LENGTH + " characters long!");
        }
        this.category = category;
        return category;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format(" Category: %s", getCategory());
    }

    @Override
    public int getWheels() {
        return wheels;
    }

    @Override
    public VehicleType getType() {
        return VehicleType.MOTORCYCLE;
    }

    @Override
    public void removeComment(Comment comment) {
        this.getComments().remove(comment);
    }

    @Override
    public void addComment(Comment comment) {
        this.getComments().add(comment);
    }
}
