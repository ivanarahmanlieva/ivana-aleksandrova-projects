package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Truck;

import java.util.List;

public class TruckImpl extends VehicleBase implements Truck {

    //look in DealershipFactoryImpl - use it to create proper constructor
    private static final int wheels = 8;
    private final int MIN_WEIGHT_CAPACITY = 0;
    private final int MAX_WEIGHT_CAPACITY = 100;
    private int weightCapacity;

    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, price, VehicleType.TRUCK, wheels);
        setWeightCapacity(weightCapacity);
    }

    public int getWeightCapacity() {
        return weightCapacity;
    }

    private int setWeightCapacity(int weightCapacity) {
        if (weightCapacity < 0) {
            throw new IllegalArgumentException("Weight capacity can not be negative!");
        }
        if (weightCapacity <= MIN_WEIGHT_CAPACITY || weightCapacity >= MAX_WEIGHT_CAPACITY) {
            throw new IllegalArgumentException("Weight capacity must be between " + MIN_WEIGHT_CAPACITY +
                    " and " + MAX_WEIGHT_CAPACITY + "!");
        }
        this.weightCapacity = weightCapacity;
        return weightCapacity;
    }


    @Override
    protected String printAdditionalInfo() {
        return String.format("Weight capacity: %st", weightCapacity);
    }

    @Override
    public int getWheels() {
        return wheels;
    }

    @Override
    public VehicleType getType() {
        return VehicleType.TRUCK;
    }


    @Override
    public void removeComment(Comment comment) {
        this.getComments().remove(comment);
    }

    @Override
    public void addComment(Comment comment) {
        this.getComments().add(comment);
    }
}
