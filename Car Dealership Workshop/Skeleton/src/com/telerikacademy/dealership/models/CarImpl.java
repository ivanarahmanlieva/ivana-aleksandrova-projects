package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Car;
import com.telerikacademy.dealership.models.contracts.Comment;


public class CarImpl extends VehicleBase implements Car {

    //look in DealershipFactoryImpl - use it to create proper constructor
    private static final int wheels = 4;
    private final int MIN_SEATS = 0;
    private final int MAX_SEATS = 10;
    private int seats;

    public CarImpl(String make, String model, double price, int seats) {
        super(make, model, price, VehicleType.CAR, wheels);
        setSeats(seats);
    }

    @Override
    public int getSeats() {
        return seats;
    }

    private int setSeats(int seats) {
        if (seats < 0) {
            throw new IllegalArgumentException("Seats must be between 0 and 10!");
        }

        if (seats <= MIN_SEATS || seats >= MAX_SEATS) {
            throw new IllegalArgumentException("Seats must be between " +
                    MIN_SEATS + " and " + MAX_SEATS + "!");
        }
        this.seats = seats;
        return seats;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format(" Seats: %d", getSeats());
    }


    @Override
    public int getWheels() {
        return wheels;
    }

    @Override
    public VehicleType getType() {
        return VehicleType.CAR;
    }

    @Override
    public void removeComment(Comment comment) {
        this.getComments().remove(comment);
    }

    @Override
    public void addComment(Comment comment) {
        this.getComments().add(comment);
    }

}
