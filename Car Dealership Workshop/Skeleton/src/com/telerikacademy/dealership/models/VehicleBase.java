package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;

import java.util.ArrayList;
import java.util.List;

public abstract class VehicleBase {

    private final static String MAKE_FIELD = "Make";
    private final static String MODEL_FIELD = "Model";
    private final static String PRICE_FIELD = "Price";
    private final static String WHEELS_FIELD = "Wheels";
    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";
    private final int MIN_MAKE_LENGTH = 2;
    private final int MAX_MAKE_LENGTH = 15;
    private final int MIN_MODEL_LENGTH = 1;
    private final int MAX_MODEL_LENGTH = 15;
    private final double MAX_PRICE = 1000000.0;


    //add fields
    private List<Comment> comments = new ArrayList<>();
    private String make;
    private String model;
    private double price;
    private int wheels;


    // finish the constructor and validate input; look in package com.telerikacademy.dealership.models.common.enums; what methods are there in VehicleType?
    public VehicleBase(String make, String model, double price, VehicleType vehicleType, int wheels) {
        setMake(make);
        setModel(model);
        setPrice(price);
        setWheels(wheels);
    }

    public String getMake() {
        return make;
    }

    private String setMake(String make) {
        if (make == null) {
            throw new NullPointerException("Make can not be null or empty!");
        }
        if (make.length() <= MIN_MAKE_LENGTH || make.length() >= MAX_MAKE_LENGTH) {
            throw new IllegalArgumentException("Make must be between " +
                    MIN_MAKE_LENGTH + " and " + MAX_MAKE_LENGTH + "characters long!");
        }
        this.make = make;
        return make;
    }

    public String getModel() {
        return model;
    }

    private String setModel(String model) {
        if (model == null) {
            throw new NullPointerException("Model can not be null or empty!");
        }
        if (model.length() <= MIN_MODEL_LENGTH || model.length() >= MAX_MODEL_LENGTH) {
            throw new IllegalArgumentException("Model length is in the range " +
                    MIN_MODEL_LENGTH + "-" + MAX_MODEL_LENGTH);
        }
        this.model = model;
        return model;
    }

    public double getPrice() {
        return price;
    }

    private double setPrice(double price) {
        if (price <= 0.0 || price > MAX_PRICE) {
            throw new IllegalArgumentException("Price must be between 0.00 and " + MAX_PRICE + "!");
        }
        this.price = price;
        return price;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%s:", this.getClass().getSimpleName().replace("Impl", ""))).append(System.lineSeparator());
        //finish implementation of toString() - what should the next 3 lines of code append to the builder?
        builder.append(String.format(" Make: %s", make)).append(System.lineSeparator());
        builder.append(String.format(" Model: %s", model)).append(System.lineSeparator());
        builder.append(String.format(" Wheels: %d", wheels)).append(System.lineSeparator());

        builder.append(String.format("  %s: $%s", PRICE_FIELD, Utils.removeTrailingZerosFromDouble(price))).append(System.lineSeparator());

        if (!printAdditionalInfo().isEmpty()) {
            builder.append(printAdditionalInfo()).append(System.lineSeparator());
        }
        builder.append(printComments());
        return builder.toString();
    }


    public int getWheels() {
        return wheels;
    }

    protected int setWheels(int wheels) {
        if (wheels < 2 || wheels > 8) {
            throw new IllegalArgumentException("Wheels can be 2,4 or 8");
        }
        this.wheels = wheels;
        return wheels;
    }

    //so it can be accessed by the subclasses in the package
    protected abstract String printAdditionalInfo();

    private String printComments() {
        StringBuilder builder = new StringBuilder();

        if (comments.size() <= 0) {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        } else {
            builder.append(String.format("%s", COMMENTS_HEADER)).append(System.lineSeparator());

            for (Comment comment : comments) {
                builder.append(comment.toString());
            }

            builder.append(String.format("%s", COMMENTS_HEADER));
        }

        return builder.toString();
    }

    public List<Comment> getComments() {
        return comments;
    }
}
